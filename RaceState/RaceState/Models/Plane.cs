﻿namespace RaceState.Models
{
    public class Plane
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}