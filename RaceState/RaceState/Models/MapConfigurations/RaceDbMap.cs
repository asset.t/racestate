﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace RaceState.Models.MapConfigurations
{
    public class RaceDbMap : IEntityTypeConfiguration<Race>
    {
        public void Configure(EntityTypeBuilder<Race> builder)
        {
            builder.HasKey(b => b.Id);
            builder.Property(b => b.Id).ValueGeneratedOnAdd().HasMaxLength(36);
            builder.Property(b => b.FromCity).HasMaxLength(32);
            builder.Property(b => b.ToCity).HasMaxLength(32);
        }
    }
}