﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace RaceState.Models.MapConfigurations
{
    public class PlaneDbMap : IEntityTypeConfiguration<Plane>
    {
        public void Configure(EntityTypeBuilder<Plane> builder)
        {
            builder.HasKey(b => b.Id);
            builder.Property(b => b.Id).ValueGeneratedOnAdd().HasMaxLength(36);
            builder.Property(b => b.Name).HasMaxLength(32);
        }
    }
}