﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace RaceState.Models.MapConfigurations
{
    public class AccountDbMap : IEntityTypeConfiguration<Account>
    {
        public void Configure(EntityTypeBuilder<Account> builder)
        {
            builder.HasKey(b => b.Id);
            builder.Property(b => b.Id).ValueGeneratedOnAdd().HasMaxLength(36);
            builder.Property(b => b.Username).HasMaxLength(32);
            builder.Property(b => b.Role).HasMaxLength(32);
        }
    }
}