﻿using Microsoft.EntityFrameworkCore;
using RaceState.Models.MapConfigurations;

namespace RaceState.Models.Data
{
    public class RaceStateContext : DbContext
    {
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Race> Races { get; set; }
        public DbSet<Plane> Planes { get; set; }
        
        public RaceStateContext(DbContextOptions<RaceStateContext> options) : base(options){}
        
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.ApplyConfiguration(new AccountDbMap());
            builder.ApplyConfiguration(new RaceDbMap());
            builder.ApplyConfiguration(new PlaneDbMap());
        }
    }
}