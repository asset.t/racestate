﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RaceState.Models
{
    public class Race
    {
        public string Id { get; set; }
        [Required(ErrorMessage = "Введите город вылета")]
        public string FromCity { get; set; }
        [Required(ErrorMessage = "Введите город прилета")]

        public string ToCity { get; set; }
        [Required(ErrorMessage = "Введите время вылета")]

        public DateTime OutTime { get; set; }
        [Required(ErrorMessage = "Введите время прилета")]

        public DateTime InTime { get; set; }
        public int DelayMinutes { get; set; }
        
        public virtual Plane Plane { get; set; }
    }
}