﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RaceState.Models;
using RaceState.Models.Data;

namespace RaceState.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class RaceController : Controller
    {
        private RaceStateContext _db;

        public RaceController(RaceStateContext db)
        {
            _db = db;
        }

        [HttpPost]
        public IActionResult Races([FromBody] int delay)
        {
            try
            {
                var races = _db.Races.Where(r => DateTime.Compare(r.OutTime.Date, DateTime.Now.Date) == 0 && r.DelayMinutes == delay)
                    .OrderBy(r => r.OutTime.AddMinutes(r.DelayMinutes)).ToList();
                return new ObjectResult(races);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return StatusCode(500);
            }
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult<Race>> Create([FromBody]Race model)
        {
            try
            {
                var race = _db.Races.FirstOrDefault(r => r.Id == model.Id);
                if (race != null)
                {
                    race.FromCity = model.FromCity;
                    race.ToCity = model.ToCity;
                    race.InTime = model.InTime;
                    race.OutTime = model.OutTime;
                    race.DelayMinutes = model.DelayMinutes;
                }

                if (ModelState.IsValid && model.Id is null)
                {
                    await _db.Races.AddAsync(model);
                }
                // if (!_db.Races.Any(r =>
                //     r.FromCity.ToLower() == race.FromCity.ToLower() && 
                //     r.ToCity.ToLower() == race.ToCity.ToLower() && 
                //     r.InTime == race.InTime &&
                //     r.OutTime == race.OutTime))
                // {
                //     await _db.Races.AddAsync(race);
                await _db.SaveChangesAsync();
                return Ok(model);
                // }

                return BadRequest();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return StatusCode(500);
            }
        }

        [HttpPut]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult<Race>> Put(Race race)
        {
            try
            {
                if (race is null)
                    return BadRequest();
                if (!_db.Races.Any(r => r.Id == race.Id))
                    return NotFound();
                _db.Races.Update(race);
                await _db.SaveChangesAsync();
                return Ok(race);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return StatusCode(500);
            }
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult<Race>> Delete(string id)
        {
            try
            {
                var race = await _db.Races.FirstOrDefaultAsync(c => c.Id == id);
                if (race is null)
                    return NotFound();
                _db.Races.Remove(race);
                await _db.SaveChangesAsync();
                return Ok(race);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return StatusCode(500);
            }
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult<Race>> Get([FromBody]RaceViewModel model)
        {
            try
            {
                var race = await _db.Races.FirstOrDefaultAsync(c => c.Id == model.Id);
                Console.WriteLine(race.Plane.Name);
                if (race is null)
                    return NotFound();
                return new ObjectResult(race);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return StatusCode(500);
            }
        }
    }
}