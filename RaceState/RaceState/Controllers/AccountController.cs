﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using RaceState.Configurations;
using RaceState.Hasher;
using RaceState.Models.Data;

namespace RaceState.Controllers
{
    public class AccountController : Controller
    {
        private RaceStateContext _db;

        public AccountController(RaceStateContext db)
        {
            _db = db;
        }

        [HttpPost("/token")]
        public IActionResult GetToken([FromBody]LoginUserModel model)
        {
            var username = model.Username;
            var password = model.Password;
            try
            {
                var identity = GetIdentity(username, PasswordHasher.GenerateHash(password));
                if (identity is null)
                {
                    return BadRequest(new {errorText = "Неправильный логин или пароль"});
                }

                var now = DateTime.UtcNow;
                var jwt = new JwtSecurityToken(
                    issuer: AuthenticationOptions.Issuer,
                    audience: AuthenticationOptions.Audience,
                    notBefore: now,
                    claims: identity.Claims,
                    expires: now.Add(TimeSpan.FromMinutes(AuthenticationOptions.Lifetime)),
                    signingCredentials: new SigningCredentials(AuthenticationOptions.GetSymmetricSecurityKey(),
                        SecurityAlgorithms.HmacSha256));
                var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

                var response = new
                {
                    access_token = encodedJwt,
                    username = identity.Name
                };

                return Json(response);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return StatusCode(500);
            }
        }

        private ClaimsIdentity GetIdentity(string username, string password)
        {
            var person = _db.Accounts.FirstOrDefault(x => x.Username == username && x.Password == password);
            if (person != null)
            {
                var claims = new List<Claim>
                {
                    new(ClaimsIdentity.DefaultNameClaimType, person.Username),
                    new(ClaimsIdentity.DefaultRoleClaimType, person.Role)
                };
                ClaimsIdentity claimsIdentity =
                    new ClaimsIdentity(claims,
                        "Token",
                        ClaimsIdentity.DefaultNameClaimType,
                        ClaimsIdentity.DefaultRoleClaimType);
                return claimsIdentity;
            }

            return null;
        }
    }
}