﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RaceState.Migrations
{
    public partial class Plane : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PlaneId",
                table: "Races",
                type: "text",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Planes",
                columns: table => new
                {
                    Id = table.Column<string>(type: "text", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Planes", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Races_PlaneId",
                table: "Races",
                column: "PlaneId");

            migrationBuilder.AddForeignKey(
                name: "FK_Races_Planes_PlaneId",
                table: "Races",
                column: "PlaneId",
                principalTable: "Planes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Races_Planes_PlaneId",
                table: "Races");

            migrationBuilder.DropTable(
                name: "Planes");

            migrationBuilder.DropIndex(
                name: "IX_Races_PlaneId",
                table: "Races");

            migrationBuilder.DropColumn(
                name: "PlaneId",
                table: "Races");
        }
    }
}
