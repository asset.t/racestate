﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RaceState.Migrations
{
    public partial class dataseed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData("Accounts", new[] {"Id", "Username", "Password", "Role"},
                new object[]
                {
                    "0e7c69fc-f30f-41db-b67e-e71b6dfeffdb", "admin@email.com", "E10ADC3949BA59ABBE56E057F20F883E",
                    "admin"
                });
            migrationBuilder.InsertData("Accounts", new[] {"Id", "Username", "Password", "Role"},
                new object[]
                {
                    "57347f3c-8d6f-4568-aa3f-1332925e1f11", "user@email.com", "C33367701511B4F6020EC61DED352059",
                    "user"
                });
            migrationBuilder.AlterColumn<string>(
                name: "ToCity",
                table: "Races",
                type: "character varying(32)",
                maxLength: 32,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "FromCity",
                table: "Races",
                type: "character varying(32)",
                maxLength: 32,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32,
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "ToCity",
                table: "Races",
                type: "character varying(32)",
                maxLength: 32,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32);

            migrationBuilder.AlterColumn<string>(
                name: "FromCity",
                table: "Races",
                type: "character varying(32)",
                maxLength: 32,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(32)",
                oldMaxLength: 32);
        }
    }
}
