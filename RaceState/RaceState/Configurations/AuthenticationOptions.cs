﻿using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace RaceState.Configurations
{
    public class AuthenticationOptions
    {
        public const string Issuer = "RaceStateAppServer";
        public const string Audience = "RaceStateAppClient";
        private const string Key = "kJw-poirU7h(6wLb";
        public const int Lifetime = 50;

        public static SymmetricSecurityKey GetSymmetricSecurityKey() 
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Key));
        }
    }
}