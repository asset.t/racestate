﻿using System.Security.Cryptography;
using System.Text;

namespace RaceState.Hasher
{
    public static class PasswordHasher
    {
        public static string GenerateHash(string password)
        {
            byte[] salt = Encoding.ASCII.GetBytes(password);
            byte[] hash = MD5.Create().ComputeHash(salt);
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                result.Append(hash[i].ToString("X2"));
            }
            return result.ToString();
        }
    }
}